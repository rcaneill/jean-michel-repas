import random
import os
from pathlib import Path
import numpy as np
from unidecode import unidecode

from database import (
    get_db,
    _reformat,
    get_only_ingredient,
    contains_month,
    contains_meat,
)

MAX_MEAT_PER_WEEK = 3

script_path = Path(os.path.dirname(os.path.abspath(__file__)))
groups = {
    "pasta_alike": {
        "ingredients": {
            "pâtes",
            "lasagne",
            "spaghetti",
            "semoule",
            "naan",
            "pizza",
            "pâte à pizza",
            "ravioli",
            "ravioli frais",
            "crozet",
            "crozets",
            "crozet sarrasin",
        },
        "weight": 0,
    },
    "rice_alike": {"ingredients": {"riz", "riz basmati", "basmati"}, "weight": 0},
    "pie_alike": {
        "ingredients": {
            "pate a tarte",
            "pate tarte",
            "pate brisée",
            "pate sablée",
            "pâte feuilletée",
        },
        "weight": 0,
    },
    "lentilles_alike": {
        "ingredients": {"lentilles vertes", "lentilles", "lentilles corail"},
        "weight": 0,
    },
    "quinoa_alike": {
        "ingredients": {"quinoa"},
        "weight": 0,
    },
}
for g in groups:
    groups[g]["ingredients"] = {
        unidecode(_reformat(i)) for i in groups[g]["ingredients"]
    }


def get_list_menus():
    return get_db()


def create_weights(all_menus):
    all_ingredients = set()
    for i in all_menus:
        all_ingredients.update(get_only_ingredient(i["recipeIngredient"]))
    all_ingredients = np.array(list(all_ingredients))

    Ingredients = np.zeros((len(all_menus), len(all_ingredients)))

    for i, m in enumerate(all_menus):
        ingredients_menu = get_only_ingredient(m["recipeIngredient"])
        Ingredients[i] = np.isin(all_ingredients, ingredients_menu)

    n = Ingredients.sum(axis=1)

    C = Ingredients @ Ingredients.T

    # For each group, we create a recipe that contains all ingredients from that group
    r = np.zeros((len(groups), len(all_ingredients)))
    for i, g in enumerate(groups):
        for j in range(len(all_ingredients)):
            r[i, j] = all_ingredients[j] in groups[g]["ingredients"]

    S = np.ones((Ingredients.shape[0],) * 2)
    for i, g in enumerate(groups):
        a = (Ingredients * r[i]).sum(axis=1)
        S[a[np.newaxis, :].T @ a[np.newaxis, :] != 0] *= groups[g]["weight"]

    N = (n[np.newaxis, :] + n[:, np.newaxis]) / 2

    w_0 = (1 - np.exp(-((C - N) ** 2) / N**2)) * S / (1 - np.exp(-1))
    return w_0


def propose_meal(db, all_menus, previous_meals, date, weights_input):
    """
    previous_meals_ids : ordered backward, i.e. item 0 is the previous one
    """
    month = date.month
    # We still need to filter per month
    good_month = [month in menu["months"] for menu in all_menus]
    ids = {int(menu["id"]): n for n, menu in enumerate(all_menus)}
    ids_keys = list(ids.keys())
    N = 20  # 4 # must be integer
    weights = np.ones((len(all_menus)))
    n_meat_last_week = 0
    for t, i in previous_meals:
        # First, if i in not in ids, we don't use it
        if int(i) not in ids_keys:
            # print(f"id {i} not in database, skip")
            continue
        try:
            time = abs((t - date).total_seconds() / 86400)  # 3600*24 = 86400
        except TypeError:
            time = abs(
                (t.replace(tzinfo=None) - date.replace(tzinfo=None)).total_seconds()
                / 86400
            )

        # We count to see if we authorize meat
        if time < 7:
            m = db[i]
            if contains_meat(get_only_ingredient(m["recipeIngredient"])):
                if time <= 0.5:
                    # we don't eat meat twice a day
                    n_meat_last_week = MAX_MEAT_PER_WEEK
                else:
                    n_meat_last_week += 1

        if time <= 2:
            time = 0.1  # And so it enforces to not get same starchy food / recipe too often
        if time <= 3:
            time *= 0.0001
        # Now we need to get the index of the previous meal in all_menus
        w = 1 - np.exp(-(time**2) / N**2) * (1 - weights_input[ids[int(i)]])
        weights *= w
        # we divide by the maximum weight to avoid hitting 0
        m = np.max(weights)
        if m == 0:
            m = 1
        weights /= m
    if n_meat_last_week >= MAX_MEAT_PER_WEEK:
        # We set a weight 0 to all meals with meat
        for i, m in enumerate(all_menus):
            if m["meat"]:
                weights[i] = 0
    # filter per month
    weights *= good_month
    return random.choices(list(all_menus), weights=weights**2)[0]


def propose_meal_week(db, previous_meals, dates, forbidden_meals=[]):
    """
    forbidden_meals is a list of meals who cannot be chosen => their weights are set to 0
    """
    all_menus = list(db.values())
    weights_input = create_weights(all_menus)
    ids = {menu["id"]: n for n, menu in enumerate(all_menus)}
    for i in forbidden_meals:
        weights_input[ids[i]] = 0
        weights_input[:, ids[i]] = 0
    n_meals = len(dates)
    meals = [None] * n_meals
    for i in range(n_meals):
        if meals[i] is None:
            meal = propose_meal(db, all_menus, previous_meals, dates[i], weights_input)
            if meal["recipeYield"] == 4:
                try:
                    meals[i + 2] = (meal["name"], meal["id"])
                except IndexError:
                    pass
            meals[i] = (meal["name"], meal["id"])
        else:
            meal = {"id": meals[i][1]}
        previous_meals.append((dates[i], meal["id"]))
    return meals
