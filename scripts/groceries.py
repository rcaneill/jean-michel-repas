import pint
import json
from database import Quantity, _reformat

filename = "groceries.json"

_groceries_in_stock = [
    _reformat(i)
    for i in [
        "Ail poudre",
        "Aneth",
        "Anis",
        "Badiane",
        "Basilic",
        "Bouillon cube",
        "Bouillon de légumes",
        "Cannelle",
        "Cardamome graine",
        "Cardamome",
        "Carvi",
        "clou girofle",
        "Colombo",
        "Coriandre",
        "Cumin",
        "Curcuma",
        "Curry",
        "Eau",
        "Épices" "Estragon",
        "Gingembre",
        "Girofle",
        "Harissa",
        "herbe provence",
        "huile",
        "huile d'olive",
        "Kororima",
        "Macis",
        "Mahaleb",
        "Masala",
        "Moutarde",
        "Muscade",
        "Oignon poudre",
        "Origan",
        "Paprika",
        "Pavot",
        "Persil",
        "Piment",
        "piment poudre",
        "Poivre",
        "Ras el Hanout",
        "Réglisse",
        "Romarin",
        "Safran",
        "Sauge",
        "Sel",
        "Sésame",
        "Thym",
    ]
]


def empty(filename=filename):
    with open(filename, "w") as f:
        f.write("{}")


def load_groceries(filename=filename):
    try:
        with open(filename, "r") as f:
            return json.load(f)
    except FileNotFoundError:
        return dict()


def write_groceries(l, filename=filename):
    with open(filename, "w") as f:
        f.write(json.dumps(l, indent=4, ensure_ascii=False))


def add_grocery(l, name, qty, l_in_stock=[]):
    if name in _groceries_in_stock:
        if name not in l_in_stock:
            l_in_stock.append(name)
        return l, l_in_stock
    if name in l:
        qs = l[name]
        added = False
        for i, q in enumerate(qs):
            try:
                q += qty
                added = True
                qs[i] = q
            except pint.errors.DimensionalityError:
                pass
        if not added:
            qs.append(qty)
        l[name] = qs
    else:
        l[name] = [qty]
    return l, l_in_stock


def list_str(l):
    for i in l.keys():
        l[i] = " ; ".join(
            [
                " ".join([i.rstrip("0").rstrip(".") for i in str(q).split(" ")])
                for q in l[i]
            ]
        )
    return l
